'''
This script will show you how to do some different things with text data.
'''
import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import brier_score_loss

from tools import *

'''
Part 1: Data import and text preprocessing
'''
# Importing the data
outdir = '~/code/ml-workshop-dev/csv/'
records = pd.read_csv(outdir + 'text.csv')

# Making a count-valued document-term matrix
vec = CountVectorizer(min_df=10, ngram_range=(1, 2))
count_mat = vec.fit_transform(records.text)

# Transforming the matrix with TF-IDF weights
trans = TfidfTransformer()
tfidf_mat = trans.fit_transform(count_mat)

# Pulling out the vocabulary
vocab = vec.vocabulary_

# Making a target variable
y = np.array(['123' in doc for doc in records.diagnosis], dtype=np.uint8)

# Prevalence calculation
flu_prev = np.sum(y) / len(y)

# Adding the target back in to the original dataset
records['asthma'] = y

'''
Part 2: Training the models
'''
# Splitting the data
seed = 10221983
n_range = range(len(records))
train, not_train = train_test_split(n_range,
                                    stratify=y,
                                    test_size=0.4,
                                    random_state=seed)
val, test = train_test_split(not_train,
                             stratify=y[not_train],
                             test_size=0.5,
                             random_state=seed)

# Training and scoring the models
rf = RandomForestClassifier(n_estimators=500)
rf.fit(tfidf_mat[train], y[train])
rf_acc = rf.score(tfidf_mat[test], y[test])

mnb = MultinomialNB()
mnb.fit(count_mat[train], y[train])
mnb_acc = mnb.score(count_mat[test], y[test])

'''
Part 3: Tuning the models
'''
# Getting predicted probabilities for the validation data
rf_val_probs = rf.predict_proba(tfidf_mat[val])[:, 1]
mnb_val_probs = mnb.predict_proba(count_mat[val])[:, 1]

# Selecting thresholds for classification
rf_gm = grid_metrics(y[val], rf_val_probs)
rf_f1_cut = rf_gm.cutoff[np.argmax(rf_gm.f1)]
rf_bc_cut = rf_gm.cutoff[np.argmin(np.abs(rf_gm.bc))]

mnb_gm = grid_metrics(y[val], mnb_val_probs)
mnb_f1_cut = mnb_gm.cutoff[np.argmax(mnb_gm.f1)]
mnb_bc_cut = mnb_gm.cutoff[np.argmin(np.abs(rf_gm.bc))]

# Getting predicted probabilities for the test data
rf_test_probs = rf.predict_proba(tfidf_mat[test])[:, 1]
mnb_test_probs = mnb.predict_proba(tfidf_mat[test])[:, 1]

# Thresholding and calculating classification metrics
rf_f1_preds = threshold(rf_test_probs, rf_f1_cut)
rf_bc_preds = threshold(rf_test_probs, rf_bc_cut)
rf_stats = pd.concat([binary_metrics(y[test], rf_f1_preds),
                      binary_metrics(y[test], rf_bc_preds)],
                     axis=0)

mnb_f1_preds = threshold(mnb_test_probs, mnb_f1_cut)
mnb_bc_preds = threshold(mnb_test_probs, mnb_bc_cut)
mnb_stats = pd.concat([binary_metrics(y[test], mnb_f1_preds),
                       binary_metrics(y[test], mnb_bc_preds)],
                      axis=0)

# Calculating Brier scores as an alternative measure
rf_brier = brier_score_loss(y[test], rf_test_probs)
mnb_brier = brier_score_loss(y[test], mnb_test_probs)

'''
Part 4: Interpreting the results
'''
# Getting the top 100 most "important" words for each classifier
rf_features = np.argsort(rf.feature_importances_)[::-1]
rf_topn = rf_features[:100]
rf_top_words = np.array([get_key(value, vocab) for value in rf_topn])

mnb_features = np.argsort(mnb.coef_.flatten())[::-1]
mnb_topn = mnb_features[:100]
mnb_top_words = np.array([get_key(value, vocab) for value in mnb_topn])

# Figuring out where the models agree and disagree
models_agree = np.intersect1d(rf_top_words, mnb_top_words)
rf_not_mnb = rf_top_words[np.where([word not in mnb_top_words
                                    for word in rf_top_words])[0]]
mnb_not_rf = mnb_top_words[np.where([word not in rf_top_words
                                     for word in mnb_top_words])[0]]
