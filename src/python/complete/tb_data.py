'''
This script will show you how to do some different things with non-text data.
'''
import numpy as np
import pandas as pd

from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, TensorBoard

from tools import *

# Importing the data
outdir = '~/code/ml-workshop-dev/csv/'
records = pd.read_csv(outdir + 'tb_bin.csv')
full = pd.read_csv(outdir + 'tb.csv')

# Separating MTB from the rest of the columns
tb = np.array(records.mtb, dtype=np.uint8)
features = np.array(records.drop('mtb', axis=1), dtype=np.uint8)
n_features = features.shape[1]
n_records = features.shape[0]

# Splitting our data into training and test sets
seed = 10221983
train, test = train_test_split(range(n_records),
                                    stratify=tb,
                                    random_state=seed,
                                    test_size=.25)

'''
Part 1: Dimension reduction
'''
# Running PCA and tranforming the raw data
pca = PCA(n_components=64)
pca_features = pca.fit_transform(features)

# Writing files to disk for visualization
metadata = records[['mtb', 'sex', 'cough', 'fever',
                    'headache', 'night_sweats']]
metadata.to_csv(outdir + 'metadata.tsv', sep='\t', index=False)
pd.DataFrame(pca_features).to_csv(outdir + 'pca_features.tsv',
                              sep='\t',
                              index=False,
                              header=None)

'''
Part 2: Using an SVM to predict TB from the 2 feature sets
'''
# Fitting an SVM
svm = LinearSVC()
svm.fit(features[train], tb[train])
svm_preds = svm.predict(features[test])
svm_stats =  binary_metrics(tb[test], svm_preds)

# And now with the PCA features
pca_svm = LinearSVC()
pca_svm.fit(pca_features[train], tb[train])
pca_preds = pca_svm.predict(pca_features[test])
pca_stats = binary_metrics(tb[test], pca_preds)

'''
Part 3: Using a network to do both tasks at once
'''
# Building and training the model
nn = DNN(input_dim=n_features,
         embedding_dim=64)
nn_stop = EarlyStopping(monitor='loss',
                        patience=30)
nn_board = TensorBoard(log_dir=outdir + 'logs/',
                       write_graph=True)
nn.compile(optimizer='adam',
           loss='binary_crossentropy')
nn.fit(features[train], 
       tb[train],
       epochs=500,
       callbacks=[nn_stop, nn_board])

# Generating and thresholding the probabilities
nn_preds = nn.predict(features[test])
nn_stats = binary_metrics(tb[test], threshold(nn_preds, 0.14))

# Using grid_metrics to pick a better threshold
nn_gm = grid_metrics(tb[test], nn_preds)
best_f1 = np.max(nn_gm.f1)

'''
Part 4: Comparing models with (Monte Carlo) cross-validation
'''
# Making a holder for the top 2 models' metrics
pca_mx = pd.DataFrame(np.zeros([10, 14]),
                      columns=pca_stats.columns)
nn_mx = pd.DataFrame(np.zeros([10, 14]),
                     columns=nn_stats.columns)

# Setting some parameters for the experiment
n_iter = 10
seeds = np.random.randint(0, 1e6, n_iter)

# Running the loop with code copied-and-pasted from earlier
for i, seed in enumerate(seeds):
    # Splitting the data
    train, test = train_test_split(range(n_records),
                              stratify=tb,
                              random_state=seed,
                              test_size=.25)
    
    # Training the SVM with PCA features
    loop_svm = LinearSVC()
    loop_svm.fit(pca_features[train], tb[train])
    loop_svm_preds = loop_svm.predict(pca_features[test])
    pca_mx.iloc[i, :] =  binary_metrics(tb[test], loop_svm_preds).values
    
    # tring the NN with binary features
    loop_nn = DNN(input_dim=n_features,
             embedding_dim=64)
    loop_stop = EarlyStopping(monitor='loss',
                            patience=30)
    loop_nn.compile(optimizer='adam',
               loss='binary_crossentropy')
    loop_nn.fit(features[train], 
           tb[train],
           epochs=500,
           verbose=0,
           callbacks=[loop_stop])
    
    # Generating and thresholding the probabilities
    loop_nn_preds = loop_nn.predict(features[test])
    prev = np.sum(tb[train]) / len(tb[train])
    nn_mx.iloc[i, :] = binary_metrics(tb[test],
                                      threshold(loop_nn_preds, prev)).values

# Running some statistical tests to compare the models; better option is
# to export _mx to CSV and analyze in stats software, e.g. R
f1_diff = np.mean(pca_mx.f1 - nn_mx.f1)
diff_diff = np.mean(np.abs(pca_mx.bc) - np.abs(nn_mx.bc))
f1_test = scipy.stats.wilcoxon(pca_mx.f1, 
                               nn_mx.f1)
diff_test = scipy.stats.wilcoxon(np.abs(pca_mx.bc), 
                                 np.abs(nn_mx.bc))
