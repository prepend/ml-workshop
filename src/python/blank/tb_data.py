'''
This script will show you how to do some different things with non-text data.
'''
import numpy as np
import pandas as pd

from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, TensorBoard

from tools import *

'''
Part 1: Dimension reduction
'''

'''
Part 2: Using an SVM to predict TB from the 2 feature sets
'''

'''
Part 3: Using a network to do both tasks at once
'''

'''
Part 4: Comparing models with (Monte Carlo) cross-validation
'''
